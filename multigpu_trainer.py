import tensorflow as tf
from tensorflow.keras.callbacks import ModelCheckpoint, TensorBoard, LearningRateScheduler
from tensorflow.keras.optimizers import SGD, Adam
import augmentation
from ssd_loss import CustomLoss
from utils import bbox_utils, data_utils, io_utils, train_utils
from models.ssd_mobilenet_v2 import get_model, init_model
from os import listdir
from os.path import isfile, join
from tensorflow.python.framework.convert_to_constants import convert_variables_to_constants_v2
import numpy as np


# NEED TO COMMENT THIS PART

gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
  # Create 2 virtual GPUs with 1GB memory each
  try:
    tf.config.experimental.set_virtual_device_configuration(
        gpus[0],
        [tf.config.experimental.VirtualDeviceConfiguration(memory_limit= 7000),
         tf.config.experimental.VirtualDeviceConfiguration(memory_limit=7000)])
    logical_gpus = tf.config.experimental.list_logical_devices('GPU')
    print(len(gpus), "Physical GPU,", len(logical_gpus), "Logical GPUs")
  except RuntimeError as e:
    # Virtual devices must be set before GPUs have been initialized
    print(e)

def parse_tfrecord(tfrecord , class_table):
  x = tf.io.parse_single_example(tfrecord, IMAGE_FEATURE_MAP)
  x_train = tf.image.decode_jpeg(x['image/encoded'] )
  x_train = tf.image.resize(x_train, (300 , 300))

  class_text = tf.sparse.to_dense(x['image/object/class/text'], default_value='')
  labels = tf.cast(class_table.lookup(class_text), tf.int32)

  y_train = tf.stack([tf.sparse.to_dense(x['image/object/bbox/xmin']),
                      tf.sparse.to_dense(x['image/object/bbox/ymin']),
                      tf.sparse.to_dense(x['image/object/bbox/xmax']),
                      tf.sparse.to_dense(x['image/object/bbox/ymax'])], axis=1)

  paddings = [[0, 100 - tf.shape(y_train)[0]], [0, 0]]
  y_train = tf.pad(y_train, paddings)

  return x_train , y_train , labels


def get_compiled_model():
    ssd_model = get_model(hyper_params)
    ssd_custom_losses = CustomLoss(hyper_params["neg_pos_ratio"], hyper_params["loc_loss_alpha"])
    ssd_model.compile(optimizer=Adam(learning_rate=1e-3),
                  loss=[ssd_custom_losses.loc_loss_fn, ssd_custom_losses.conf_loss_fn])
    init_model(ssd_model)
    
    return ssd_model


def get_dataset():
    batch_size = 32
    hyper_params = train_utils.get_hyper_params(backbone)
    prior_boxes = bbox_utils.generate_prior_boxes(hyper_params["feature_map_shapes"], hyper_params["aspect_ratios"])
    ssd_train_feed = train_utils.generator(train_data, prior_boxes, hyper_params)
    
    return ( ssd_train_feed)


batch_size = 32
epochs = 25
load_weights = False
with_voc_2012 = True
backbone = "mobilenet_v2"
io_utils.is_valid_backbone(backbone)


hyper_params = train_utils.get_hyper_params(backbone)


#Labels for VOC2012 dataset
#labels = [ 'aeroplane', 'bicycle', 'bird', 'boat', 'bottle', 'bus', 'car',
#            'cat', 'chair', 'cow', 'diningtable', 'dog', 'horse', 'motorbike', 'person',
#            'pottedplant', 'sheep', 'sofa', 'train', 'tvmonitor']
#labels = ["bg"] + labels

len_dataset = 0
with open('dataset.names', 'r') as f:
    data = f.read()
    len_dataset = len(list(data.split()))

hyper_params["total_labels"] = len_dataset
img_size = hyper_params["img_size"]



mypath = 'tfRecords'
onlyfiles = ['tfRecords/'+f for f in listdir(mypath) if isfile(join(mypath, f))]


class_file = 'dataset.names'
LINE_NUMBER = -1


files = tf.data.Dataset.list_files(onlyfiles)
dataset = files.flat_map(tf.data.TFRecordDataset)


len_whole_data = len(list(dataset))

print('Length of Dataset : ',len_whole_data)

# !--- UNCOMMENT IN CASE OF SINGLE TF RECORD FILE 
# file_pattern = 'voc2012.tfrecord'
# files = tf.data.Dataset.list_files(file_pattern)
# dataset = files.flat_map(tf.data.TFRecordDataset)

class_table = tf.lookup.StaticHashTable(tf.lookup.TextFileInitializer(class_file, tf.string, 0, tf.int64, LINE_NUMBER, delimiter="\n"), -1)


IMAGE_FEATURE_MAP = {
    'image/encoded': tf.io.FixedLenFeature([], tf.string),
    'image/object/bbox/xmin': tf.io.VarLenFeature(tf.float32),
    'image/object/bbox/ymin': tf.io.VarLenFeature(tf.float32),
    'image/object/bbox/xmax': tf.io.VarLenFeature(tf.float32),
    'image/object/bbox/ymax': tf.io.VarLenFeature(tf.float32),
    'image/object/class/text': tf.io.VarLenFeature(tf.string),
}

data = dataset.map(lambda x: parse_tfrecord(x , class_table))
data_shapes = data_utils.get_data_shapes()
padding_values = data_utils.get_padding_values()
train_data = data.shuffle(batch_size*4).padded_batch(batch_size, padded_shapes=data_shapes, padding_values=padding_values)


strategy = tf.distribute.MirroredStrategy()
print("Number of devices: {}".format(strategy.num_replicas_in_sync))

train_data_set = get_dataset()

step_size_train = train_utils.get_step_size(len_whole_data, batch_size)
# step_size_val = train_utils.get_step_size(1144, batch_size)

with strategy.scope():
  ssd_model = get_compiled_model()
  train_data_set = get_dataset()

ssd_model.fit(train_data_set,steps_per_epoch = step_size_train, epochs= epochs )

ssd_model.save_weights("ssd_saved_model.h5")


#UNCOMMENT THIS FOR SAVING THE FROZEN GRAPH



# frozen_out_path = '/frozenGraph/'
# frozen_graph_filename = 'SSD_Lite_inference_frozen'

# full_model = tf.function(lambda x: ssd_model(x))
# full_model = full_model.get_concrete_function(
#     tf.TensorSpec(ssd_model.inputs[0].shape, ssd_model.inputs[0].dtype))

# # Get frozen ConcreteFunction

# frozen_func = convert_variables_to_constants_v2(full_model)
# frozen_func.graph.as_graph_def()
# layers = [op.name for op in frozen_func.graph.get_operations()]

# print("-" * 60)
# print("Frozen model layers: ")

# for layer in layers:
#     print(layer)
# print("-" * 60)
# print("Frozen model inputs: ")
# print(frozen_func.inputs)
# print("Frozen model outputs: ")
# print(frozen_func.outputs)
# # Save frozen graph to disk

# tf.io.write_graph(graph_or_graph_def=frozen_func.graph,
#                   logdir=frozen_out_path,
#                   name=f"{frozen_graph_filename}.pb",
#                   as_text=False)

# # Save its text representation
# tf.io.write_graph(graph_or_graph_def=frozen_func.graph,
#                   logdir=frozen_out_path,
#                   name=f"{frozen_graph_filename}.pbtxt",
#                   as_text=True)



