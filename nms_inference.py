import tensorflow as tf
from tensorflow.keras.layers import Layer, Input, Conv2D, MaxPool2D
from tensorflow.keras.models import Model
from utils import bbox_utils, data_utils, io_utils, train_utils, eval_utils
from models.ssd_mobilenet_v2 import get_model, init_model
import time 

class SSDDecoder(Layer):
    """Generating bounding boxes and labels from ssd predictions.
    First calculating the boxes from predicted deltas and label probs.
    Then applied non max suppression and selecting top_n boxes by scores.
    inputs:
        pred_deltas = (batch_size, total_prior_boxes, [delta_y, delta_x, delta_h, delta_w])
        pred_label_probs = (batch_size, total_prior_boxes, [0,0,...,0])
    outputs:
        pred_bboxes = (batch_size, top_n, [y1, x1, y2, x2])
        pred_labels = (batch_size, top_n)
            1 to total label number
        pred_scores = (batch_size, top_n)
    """

    def __init__(self, prior_boxes, variances, max_total_size=20, score_threshold=0.1, **kwargs):
        super(SSDDecoder, self).__init__(**kwargs)
        self.prior_boxes = prior_boxes
        self.variances = variances
        self.max_total_size = max_total_size
        self.score_threshold = score_threshold

    def get_config(self):
        config = super(SSDDecoder, self).get_config()
        config.update({
            "prior_boxes": self.prior_boxes.numpy(),
            "variances": self.variances,
            "max_total_size": self.max_total_size,
            "score_threshold": self.score_threshold
        })
        return config

    def call(self, inputs):
        pred_deltas = inputs[0]
        pred_label_probs = inputs[1]
        batch_size = tf.shape(pred_deltas)[0]
        #
        pred_deltas *= self.variances
        pred_bboxes = bbox_utils.get_bboxes_from_deltas(self.prior_boxes, pred_deltas)
        #
        pred_labels_map = tf.expand_dims(tf.argmax(pred_label_probs, -1), -1)
        pred_labels = tf.where(tf.not_equal(pred_labels_map, 0), pred_label_probs, tf.zeros_like(pred_label_probs))
        # Reshape bboxes for non max suppression
        pred_bboxes = tf.reshape(pred_bboxes, (batch_size, -1, 1, 4))
        #
        final_bboxes, final_scores, final_labels, _ = bbox_utils.non_max_suppression(
                                                                    pred_bboxes, pred_labels,
                                                                    max_output_size_per_class=self.max_total_size,
                                                                    max_total_size=self.max_total_size,
                                                                    score_threshold=self.score_threshold)
        #
        return final_bboxes, final_labels, final_scores

def get_decoder_model(base_model, prior_boxes, hyper_params):
    """Decoding ssd predictions to valid bounding boxes and labels.
    inputs:
        base_model = tf.keras.model, base ssd model
        prior_boxes = (total_prior_boxes, [y1, x1, y2, x2])
            these values in normalized format between [0, 1]
        hyper_params = dictionary

    outputs:
        ssd_decoder_model = tf.keras.model
    """
    bboxes, classes, scores = SSDDecoder(prior_boxes, hyper_params["variances"])(base_model.output)
    return Model(inputs=base_model.input, outputs=[bboxes, classes, scores])



batch_size = 1
evaluate = True
use_custom_images = False
custom_image_path = "TestImages/"
backbone = "mobilenet_v2"

labels = [ 'aeroplane', 'bicycle', 'bird', 'boat', 'bottle', 'bus', 'car',
 'cat', 'chair', 'cow', 'diningtable', 'dog', 'horse', 'motorbike', 'person',
 'pottedplant', 'sheep', 'sofa', 'train', 'tvmonitor']
labels = ["bg"] + labels
hyper_params = train_utils.get_hyper_params(backbone)

hyper_params["total_labels"] = len(labels)
img_size = hyper_params["img_size"]

data_types = data_utils.get_data_types()
data_shapes = data_utils.get_data_shapes()
padding_values = data_utils.get_padding_values()

img_paths = data_utils.get_custom_imgs(custom_image_path)
total_items = len(img_paths)

print(img_paths , img_size)
test_data = tf.data.Dataset.from_generator(lambda: data_utils.custom_data_generator(img_paths, img_size, img_size), data_types, data_shapes)


# test_data = test_data.map(lambda x : data_utils.preprocessing(x, img_size, img_size, evaluate=evaluate))
test_data = test_data.padded_batch(batch_size, padded_shapes=data_shapes, padding_values=padding_values)

ssd_model = get_model(hyper_params)
ssd_model_path = io_utils.get_model_path(backbone)
ssd_model.load_weights("ssd_saved_model.h5")


# t1 = time.time()
# boxes_1, scores_1, classes_1, nums_1 = yolo.predict(dataset.take(1))
# t2 = time.time()
# print(f'TIME TAKEN FOR FIRST IMAGE INFERENCE:{t2-t1}')

# t3 = time.time()
# boxes, scores, classes, nums = yolo.predict(dataset.skip(1))
# t4 = time.time()

prior_boxes = bbox_utils.generate_prior_boxes(hyper_params["feature_map_shapes"], hyper_params["aspect_ratios"])
ssd_decoder_model = get_decoder_model(ssd_model, prior_boxes, hyper_params)
step_size = train_utils.get_step_size(total_items, batch_size)


# print(test_data)
# t1 = time.time()
# pred_bboxes, pred_labels, pred_scores = ssd_decoder_model.predict(test_data.take(1), steps=step_size, verbose=1)
# t2 = time.time()

print("\n\n -----------INFERENCE----------------\n\n")


# print(t2-t1 , "JUST SKIPPED THE 1st IMAGE------------------ \n\n")
i = 0
for element in test_data:

  t1 = time.time()
  pred_bboxes, pred_labels, pred_scores = ssd_decoder_model.predict(element)
  t2 = time.time()
  print( "Time for ",i ," is :   " , t2-t1)

  i += 1

# t1 = time.time()
# pred_bboxes, pred_labels, pred_scores = ssd_decoder_model.predict(test_data.skip(1), steps=step_size, verbose=1)
# t2 = time.time()

# print(t2-t1 , "Main")



























