import tensorflow as tf
from utils import bbox_utils, data_utils, drawing_utils, io_utils, train_utils, eval_utils
from models.decoder import get_decoder_model
from models.ssd_mobilenet_v2 import get_model, init_model
import time 

def parse_tfrecord(tfrecord , class_table):
  x = tf.io.parse_single_example(tfrecord, IMAGE_FEATURE_MAP)
  x_train = tf.image.decode_jpeg(x['image/encoded'] )
  x_train = tf.image.resize(x_train, (300 , 300))

  class_text = tf.sparse.to_dense(x['image/object/class/text'], default_value='')
  # print(class_text)
  labels = tf.cast(class_table.lookup(class_text), tf.int32)

  y_train = tf.stack([tf.sparse.to_dense(x['image/object/bbox/ymin']),
                      tf.sparse.to_dense(x['image/object/bbox/xmin']),
                      tf.sparse.to_dense(x['image/object/bbox/ymax']),
                      tf.sparse.to_dense(x['image/object/bbox/xmax'])], axis=1)
  
  paddings = [[0, 100 - tf.shape(y_train)[0]], [0, 0]]
  y_train = tf.pad(y_train, paddings)
  return x_train , y_train , labels



batch_size = 1
evaluate = True
backbone = 'mobilenet_v2'
hyper_params = train_utils.get_hyper_params(backbone)

IMAGE_FEATURE_MAP = {
    'image/encoded': tf.io.FixedLenFeature([], tf.string),
    'image/object/bbox/xmin': tf.io.VarLenFeature(tf.float32),
    'image/object/bbox/ymin': tf.io.VarLenFeature(tf.float32),
    'image/object/bbox/xmax': tf.io.VarLenFeature(tf.float32),
    'image/object/bbox/ymax': tf.io.VarLenFeature(tf.float32),
    'image/object/class/text': tf.io.VarLenFeature(tf.string),
}

class_file = 'dataset.names'
LINE_NUMBER = -1
class_table = tf.lookup.StaticHashTable(tf.lookup.TextFileInitializer(class_file, tf.string, 0, tf.int64, LINE_NUMBER, delimiter="\n"), -1)

labels = [ 'aeroplane', 'bicycle', 'bird', 'boat', 'bottle', 'bus', 'car',
 'cat', 'chair', 'cow', 'diningtable', 'dog', 'horse', 'motorbike', 'person',
 'pottedplant', 'sheep', 'sofa', 'train', 'tvmonitor']
labels = ["bg"] + labels

hyper_params["total_labels"] = len(labels)
img_size = hyper_params["img_size"]

data_types = data_utils.get_data_types()
data_shapes = data_utils.get_data_shapes()
padding_values = data_utils.get_padding_values()

file_pattern = 'testData.tfrecord'
files = tf.data.Dataset.list_files(file_pattern)
dataset = files.flat_map(tf.data.TFRecordDataset)
test_data = dataset.map(lambda x: parse_tfrecord(x , class_table))
test_data = test_data.padded_batch(batch_size, padded_shapes=data_shapes, padding_values=padding_values)

total_items = len(files)

# test_data = test_data.map(lambda x : data_utils.preprocessing(x, img_size, img_size, evaluate=evaluate))
# test_data = test_data.padded_batch(batch_size, padded_shapes=data_shapes, padding_values=padding_values)

ssd_model = get_model(hyper_params)
ssd_model_path = io_utils.get_model_path(backbone)
ssd_model.load_weights("ssd_saved_model.h5")

prior_boxes = bbox_utils.generate_prior_boxes(hyper_params["feature_map_shapes"], hyper_params["aspect_ratios"])
ssd_decoder_model = get_decoder_model(ssd_model, prior_boxes, hyper_params)
step_size = train_utils.get_step_size(total_items, 1)

pred_bboxes, pred_labels, pred_scores = ssd_decoder_model.predict(test_data, verbose=1)

stats , mAp = eval_utils.evaluate_predictions(test_data, pred_bboxes, pred_labels, pred_scores, labels, batch_size)

print(mAp)
