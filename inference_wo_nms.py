import time
import os
import cv2

import numpy as np
import tensorflow as tf
from tensorflow.keras.layers import Layer, Input, Conv2D, MaxPool2D
from tensorflow.keras.models import Model
from utils import bbox_utils, data_utils, io_utils, train_utils, eval_utils
from models.ssd_mobilenet_v2 import get_model, init_model


PATH_TO_WEIGHTS = 'trained_weight/weight'
SIZE = 300
PATH_TO_DIR = 'frames_vlc-record-2020-03-17-14h24m11s-GH010168/'
PATH_TO_OUTPUT = 'white_output/'
NUM_CLASSES = 1

backbone = "mobilenet_v2"

hyper_params = train_utils.get_hyper_params(backbone)
hyper_params["total_labels"] = 2

prior_boxes = bbox_utils.generate_prior_boxes(hyper_params["feature_map_shapes"], hyper_params["aspect_ratios"])

class SSDDecoder(Layer):
    """Generating bounding boxes and labels from ssd predictions.
    First calculating the boxes from predicted deltas and label probs.
    Then applied non max suppression and selecting top_n boxes by scores.
    inputs:
        pred_deltas = (batch_size, total_prior_boxes, [delta_y, delta_x, delta_h, delta_w])
        pred_label_probs = (batch_size, total_prior_boxes, [0,0,...,0])
    outputs:
        pred_bboxes = (batch_size, top_n, [y1, x1, y2, x2])
        pred_labels = (batch_size, top_n)
            1 to total label number
        pred_scores = (batch_size, top_n)
    """

    def __init__(self, prior_boxes, variances, max_total_size=20, score_threshold=0.1, **kwargs):
        super(SSDDecoder, self).__init__(**kwargs)
        self.prior_boxes = prior_boxes
        self.variances = variances
        self.max_total_size = max_total_size
        self.score_threshold = score_threshold

    def get_config(self):
        config = super(SSDDecoder, self).get_config()
        config.update({
            "prior_boxes": self.prior_boxes.numpy(),
            "variances": self.variances,
            "max_total_size": self.max_total_size,
            "score_threshold": self.score_threshold
        })
        return config

    def call(self, inputs):
        pred_deltas = inputs[0]
        pred_label_probs = inputs[1]
        batch_size = tf.shape(pred_deltas)[0]
        #
        pred_deltas *= self.variances
        pred_bboxes = bbox_utils.get_bboxes_from_deltas(self.prior_boxes, pred_deltas)
        #
        pred_labels_map = tf.expand_dims(tf.argmax(pred_label_probs, -1), -1)
        pred_labels = tf.where(tf.not_equal(pred_labels_map, 0), pred_label_probs, tf.zeros_like(pred_label_probs))
        # Reshape bboxes for non max suppression
        pred_bboxes = tf.reshape(pred_bboxes, (batch_size, -1, 1, 4))
        #
        final_bboxes, final_scores, final_labels, _ = bbox_utils.non_max_suppression(
                                                                    pred_bboxes, pred_labels,
                                                                    max_output_size_per_class=self.max_total_size,
                                                                    max_total_size=self.max_total_size,
                                                                    score_threshold=self.score_threshold)
        #
        return final_bboxes, final_labels, final_scores

def get_custom_imgs(custom_image_path):
    img_paths = []
    for path, dir, filenames in os.walk(custom_image_path):
        for filename in filenames:
            img_paths.append(os.path.join(path, filename))
        break
    print(len(img_paths))
    return img_paths

def draw_outputs(img, outputs):
    boxes, objectness, classes, nums = outputs
    boxes, objectness, classes, nums = boxes[0], objectness[0], classes[0], 20
    wh = np.flip(img.shape[0:2])

    for i in range(nums):
        x1y1 = tuple((np.array(boxes[i][0:2]) * wh).astype(np.int32))
        x2y2 = tuple((np.array(boxes[i][2:4]) * wh).astype(np.int32))
        img = cv2.rectangle(img, x1y1, x2y2, (255, 0, 0), 2)
        img = cv2.putText(img, '{} {:.4f}'.format(
            'case', objectness[i]),
            x1y1, cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0, 0, 255), 2)
    return img

def transform_images(x_train, size):
    x_train = tf.image.resize(x_train, (size, size))
    x_train = x_train / 255
    return x_train


# def main():

ssd_model = get_model(hyper_params)
ssd_model_path = io_utils.get_model_path(backbone)
ssd_model.load_weights(PATH_TO_WEIGHTS)
print("WEIGHTS HAVE BEEN LOADED!")
ssd_model.summary()

img_paths = get_custom_imgs(PATH_TO_DIR)
for img_path in img_paths:
    image = cv2.imread(img_path)
    img = np.array(image)
    img = tf.expand_dims(img, 0)
    img = transform_images(img, SIZE)
    t3 = time.time()
    delta , labels = ssd_model.predict(img)
    t4 = time.time()
    print(f'Time taken for MODEL PREDIDCTION on this image: {t4-t3}')
    t5 = time.time()
    bboxes, classes, scores = SSDDecoder(prior_boxes, hyper_params["variances"])([delta , labels])
    t6 = time.time()
    print(f'Time taken for NMS on this image: {t6-t5}')

    # for i in range(nums[0]):
    #   print(f'\t{np.array(scores[0][i])},{np.array(boxes[0][i])}')

    img = draw_outputs(np.array(image), (bboxes, scores, classes, 20))
    cv2.imwrite(os.path.join(PATH_TO_OUTPUT,img_path.split('/')[-1]), img)
